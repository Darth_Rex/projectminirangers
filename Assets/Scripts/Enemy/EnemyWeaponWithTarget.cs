﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class EnemyWeaponWithTarget : EnemyWeapon {
		public float fieldOfView = 45f;
		public Transform targetToShoot {get; private set;}

		public override void Awake () {
			base.Awake ();
			var ps = FindObjectOfType<PlayerShip>();
			if(ps != null) {
				targetToShoot = ps.transform;
			}
		}

		public override void Shoot () {
			if(targetToShoot != null) {
				if(Vector3.Angle(_transform.up, targetToShoot.transform.position - _transform.position) <= fieldOfView) {
					Shoot(targetToShoot.position - _transform.position);
					return;
				}
			}
			base.Shoot();
		}
	}
}