﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class EnemyShip : Ship {

		public override void OnSpawn() {
			base.OnSpawn ();
			ResetStats();
			_rigidbody.velocity = _transform.up * speed;
		}

		public override void Destroy () {
			base.Destroy ();
			Game.Instance.scoreManager.AddScore((uint)Mathf.RoundToInt(maxHitPoints) * 10);
		}
	}
}