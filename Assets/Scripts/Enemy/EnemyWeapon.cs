﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class EnemyWeapon : Weapon {

		public override void Awake () {
			base.Awake ();
			_isShooting = true;
		}
	}
}