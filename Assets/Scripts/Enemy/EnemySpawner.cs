﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class EnemySpawner : MonoBehaviour {

		public float spawnRate;
		public GameObject[] enemies;
		private float _spawnedTime;
	    private bool _isSpawn = true;
	    private Boundaries _gameArena;
		private Quaternion _initialShipRotation;

		void Start() {
			_spawnedTime = Time.time;
		    Game.Instance.GameEnded += OnGameEnded;
            _gameArena = new Boundaries();
			_initialShipRotation = Quaternion.Euler(-Vector3.right * 180f);
		}

		void Update() {
            if (_isSpawn && Time.time > _spawnedTime) {
				_spawnedTime = Time.time + spawnRate;
                var pos = new Vector2(Random.Range(_gameArena.left, _gameArena.right), _gameArena.top);
				GetRandomEnemyPrefab().Spawn(pos, _initialShipRotation);
			}
		}

	    void OnDestroy() {
			if(Game.isAccesable) {
            	Game.Instance.GameEnded -= OnGameEnded;
			}
        }

		GameObject GetRandomEnemyPrefab() {
			if(enemies.Length == 0) {
				return ObjectPoolCollection.Instance.GetObjectPoolPrefab(CollectionEntityType.EnemyShip);
			}
			return enemies[Random.Range(0, enemies.Length)];
		}

		void OnTriggerExit2D(Collider2D other) {
			var go = other.gameObject;
			if(go.layer == Layers.Enemy || go.layer == Layers.Projectile) {
				go.Recycle();
			}
		}

	    void OnGameEnded() {
	        _isSpawn = false;
	    }
	}
}