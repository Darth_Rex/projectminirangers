﻿using UnityEngine;
using UnityEngine.UI;

namespace MiniRangers {
	public class HUD : MonoBehaviour {

		public Text scoreLabel;
		public GameObject pauseButtonObject;
		public UIBaseWindow menuWindow;
		public UIBaseWindow gameOverWindow;

		private Game _game;
		private Controlls _controlls;

		public void Menu() {
			_game.Pause(true);
			pauseButtonObject.SetActive(false);
			menuWindow.Show();
		}

		public void Resume() {
			_game.Pause(false);
			pauseButtonObject.SetActive(true);
			menuWindow.Hide();
		}

		public void Exit() {
			_game.MainMenu();
		}

		void Start () {
			_game = Game.Instance;
			menuWindow.Hide();
			gameOverWindow.Hide();
			_game.scoreManager.ScoreUpdate += OnScoreUpdate;
			_controlls = Controlls.Instance;
			_controlls.Menu += Menu;
			_game.GameEnded += OnGameOver;
		}

		void OnGameOver() {
			pauseButtonObject.SetActive(false);
			gameOverWindow.Show();
			_controlls.Menu -= Menu;
		}

		void OnDestroy() {
			_game.scoreManager.ScoreUpdate -= OnScoreUpdate;
			_controlls.Menu -= Menu;
			_game.GameEnded -= OnGameOver;
		}

		void OnScoreUpdate(uint score) {
			scoreLabel.text = "Score: " + score;
		}

		void OnMenu() {
			if(_game.isPaused) Resume();
			else Menu ();
		}
	}
}
