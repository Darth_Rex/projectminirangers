﻿using UnityEngine;
using UnityEngine.UI;

namespace MiniRangers {
	public class UIMainMenu : MonoBehaviour {

		public Button exitButton;
		public Text welcomeText;
		public InputField enterNameField;
		public Button enterNameButton;
		public GameObject enterNewNameWindow;

		private Game _game;

		void Start() {
			_game = Game.Instance;
	#if UNITY_IOS || UNITY_ANDROID || UNITY_WEB || UNITY_EDITOR
			if(exitButton != null) {
				exitButton.gameObject.SetActive(false);
			}
	#endif
			_game.UserUpdated += UpdateWelcomeText;
			enterNameButton.onClick.AddListener(() => EnterNewName(enterNameField.text));
			enterNewNameWindow.SetActive(false);
			UpdateWelcomeText(_game.userProfile);
		}

		public void ShowEnterNameWindow() {
			enterNewNameWindow.SetActive(true);
			if(enterNameField.text.Equals("")) {
				enterNameField.text = Game.DEFAULT_USER_NAME;
			}
		}

		public void EnterNewName(string name) {
			_game.SetUser(name);
			enterNewNameWindow.SetActive(false);
		}

		public void NewGame() {
			_game.NewGame();
		}

		public void HighScore() {
			_game.ShowHightScore();
		}

		public void Exit() {
			Application.Quit();
		}

		void UpdateWelcomeText(UserProfile newProfile) {
			welcomeText.text = "Welcome, " + newProfile.name;
		}

		void OnDestroy() {
			_game.UserUpdated -= UpdateWelcomeText;
		}
	}
}