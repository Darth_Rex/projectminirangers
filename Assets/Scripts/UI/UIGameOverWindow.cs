﻿using UnityEngine;
using UnityEngine.UI;

namespace MiniRangers {
	public class UIGameOverWindow : UIBaseWindow {

		public Text scoreLabel;

		public override void Show() {
			base.Show();
			scoreLabel.text = Game.Instance.scoreManager.score.ToString();
		}
	}
}