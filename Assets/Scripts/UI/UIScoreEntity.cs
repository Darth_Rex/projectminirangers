﻿using UnityEngine;
using UnityEngine.UI;

namespace MiniRangers {
	public class UIScoreEntity : MonoBehaviour {

		public Text positionLabel;
		public Text nameLabel;
		public Text scoreLabel;

		private ScoreEntity _score;

		public void SetScoreEntity(ScoreEntity entity, int atPosition) {
			_score = entity;
			positionLabel.text = atPosition.ToString() + ".";
			nameLabel.text = _score.name;
			scoreLabel.text = _score.score.ToString();
		}
	}
}