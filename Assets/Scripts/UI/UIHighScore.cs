﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace MiniRangers {
	public class UIHighScore : MonoBehaviour {

		public Text noScoreLabel;
		public List<UIScoreEntity> leaderBoard;

		public void ReturnToMainMenu () {
			Game.Instance.MainMenu();
		}

		void Start() {
			var lb = Game.Instance.scoreManager.GetLeaderBoard();
			InitWithList(lb);
		}

		void InitWithList(IList<ScoreEntity> sortedScore) {
			if(sortedScore.Count == 0) {
				noScoreLabel.gameObject.SetActive(true);
			}

			int i = 0;
			for(; i < leaderBoard.Count && i < sortedScore.Count; ++i) {
				var scoreEntity = leaderBoard[i];
				scoreEntity.gameObject.SetActive(true);
				scoreEntity.SetScoreEntity(sortedScore[i], i+1);
			}

			for(; i < leaderBoard.Count; ++i) {
				leaderBoard[i].gameObject.SetActive(false);
			}
		}
	}
}