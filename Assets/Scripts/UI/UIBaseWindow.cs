﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class UIBaseWindow : MonoBehaviour, IWindow {

		public virtual void Show () {
			gameObject.SetActive(true);
		}
		
		public virtual void Hide () {
			gameObject.SetActive(false);
		}
	}
}