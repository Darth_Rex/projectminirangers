﻿using UnityEditor;
using UnityEngine;

public class EditorTools {
	
	[MenuItem("Tools/Clean Save")]
	static void CleanSave() {
		System.IO.File.Delete(SaveManager.GetSavePath());
	}
}