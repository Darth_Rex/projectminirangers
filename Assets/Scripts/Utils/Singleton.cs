﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	public static bool isAccesable { get { return !_applicationIsQuitting; } }
	protected static bool _applicationIsQuitting = false;
	private static T _instance;
	private static object _lock = new object();
	private static bool _addingComponent = false;

	public static T Instance {
		get {
			if (_applicationIsQuitting) {
				Debug.LogWarning("[Singleton] Application is quiting. Returning null for " + typeof(T));
				return null;
			}

			lock(_lock) {
				if (_instance == null) {
					_instance = FindObjectOfType<T>();
					
					if (_instance == null) {
						var prefab = Resources.Load("Prefabs/Singleton/" + typeof(T).Name);
						if(prefab != null) {
							var singletonObject = Instantiate(prefab) as GameObject;
							_instance = singletonObject.GetComponent<T>();
						}
						else {
							var singleton = new GameObject(typeof(T).Name);
							_addingComponent = true;
							_instance = singleton.AddComponent<T>(); 
							_addingComponent = false;
						}
						Debug.Log("[Singleton] Created " + typeof(T) + " instanceID = " + _instance.GetInstanceID());
					}
					else {
						Debug.Log("[Singleton] Found one in scene: " + _instance.gameObject.name + " instanceID = " + _instance.GetInstanceID());
					}
				}
				return _instance;
			}
		}
	}
	
	public virtual void Awake() {
		if(!_addingComponent && Instance != this) {
			Destroy(gameObject);
			return;
		}
		DestroyProtection();
	}

	public virtual void OnApplicationQuit() {
		_applicationIsQuitting = true;
	}

	public virtual void OnDestroy() {
		_instance = null;
	}

	protected virtual void DestroyProtection() {
		DontDestroyOnLoad(this);
	}
}