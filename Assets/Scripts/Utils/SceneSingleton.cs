﻿using UnityEngine;
using System.Collections;

public class SceneSingleton<T> : Singleton<T> where T : MonoBehaviour {
	protected override void DestroyProtection () {
		Debug.Log("[Singleton]: " + this.name + " will be destroyed after leaving current scene");
	}
}