﻿using UnityEngine;
using System;

namespace MiniRangers {
	[RequireComponent (typeof(Rigidbody2D))]
	public class PlayerController : MonoBehaviour {

		public float speed;
		public Projectile projectile;

		public delegate void MoveDelegate(Vector2 velocity);
		public event MoveDelegate Move;
		public Action Fire;
		public Action Menu;

		void Start() {
		}

		void FixedUpdate() {
			var horizontalAxis = Input.GetAxis("Horizontal");
			var verticalAxis = Input.GetAxis("Vertical");
			var movement = new Vector2(horizontalAxis, verticalAxis);

			if(Move != null) {
				Move(movement);
			}

			/*var movement = new Vector2(horizontalAxis, verticalAxis);
			_rigidbody.velocity = movement * speed;
			//_rigidbody.position = new Vector2 (_rigidbody.position.x, _rigidbody.position.y);
			_rigidbody.position = new Vector2
				(
				Mathf.Clamp (_rigidbody.position.x, _mapBoundaries.left, _mapBoundaries.right), 
				Mathf.Clamp (_rigidbody.position.y, _mapBoundaries.bottom, _mapBoundaries.top)
				);*/
		}

		void Update() {
			if(Input.GetKeyDown(KeyCode.Space)) {
				if(Fire != null) {
					Fire();
				}
			}

			if(Input.GetKeyUp(KeyCode.Escape)) {
				if(Menu != null) {
					Menu();
				}
			}

			/*if(Input.GetKeyDown(KeyCode.Space) && Time.time >= _fireCountdown) {
				_fireCountdown = Time.time + fireRate;
				projectile.gameObject.Spawn(transform.position, _transform.rotation);
			}*/
		}
	}
}