﻿using UnityEngine;
using System;

namespace MiniRangers {
	public class Controlls: Singleton<Controlls> {

		public delegate void MoveDelegate(Vector2 velocity);
		public event MoveDelegate Move;
		public Action StartFire;
		public Action StopFire;
		public Action Menu;

		private Game _game;
		private bool _fireing;

		public override void Awake () {
			base.Awake ();
			_game = Game.Instance;
			_game.Paused += OnPause;
		}

		void FixedUpdate() {
			if(_game.isPaused) return;
			if(Move != null) {
				var horizontalAxis = Input.GetAxis("Horizontal");
				var verticalAxis = Input.GetAxis("Vertical");
				var movement = new Vector2(horizontalAxis, verticalAxis);
				Move(movement);
			}
		}
		
		void Update() {
			if(Input.GetKeyUp(KeyCode.Escape)) {
				Debug.Log ("UP");
				if(Menu != null) {
					Menu();
				}
			}

			// Pause depended inputs
			if(_game.isPaused) return;

			if(Input.GetKeyDown(KeyCode.Space)) {
				if(StartFire != null) {
					_fireing = true;
					StartFire();
				}
			}

			if(Input.GetKeyUp(KeyCode.Space) && _fireing) {
				_fireing = false;
				if(StopFire != null) {
					StopFire();
				}
			}
		}

		void OnPause(bool paused) {
			if(paused && _fireing) {
				_fireing = false;
				if(StopFire != null) {
					StopFire();
				}
			}
		}
	}
}