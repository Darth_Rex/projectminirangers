﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class PlayerWeapon : Weapon, IControllable {

		protected Controlls _controlls;

		public void Start() {
			_controlls = Controlls.Instance;
			Subscribe();
		}

		public void Subscribe() {
			_controlls.StartFire += StartFire;
			_controlls.StopFire += StopFire;
		}

		public void Unsubscribe() {
			_controlls.StartFire -= StartFire;
			_controlls.StopFire -= StopFire;
		}

		public void OnDestroy() {
			Unsubscribe();
		}

		private void StartFire() {
			_isShooting = true;
		}

		private void StopFire() {
			_isShooting = false;
		}
	}
}