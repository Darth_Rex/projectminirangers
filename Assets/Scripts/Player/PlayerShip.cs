﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class PlayerShip : Ship, IControllable {

		protected Boundaries _mapBoundaries;
		protected Controlls _controlls;

		public override void Start () {
			base.Start ();
			_mapBoundaries = new Boundaries(GetComponent<BoxCollider2D>());
			Debug.Log(_mapBoundaries.ToString());
			_controlls = Controlls.Instance;
			Subscribe();
		}

		void Movement(Vector2 movement) {
			_rigidbody.velocity = movement * speed;
			var xPos = Mathf.Clamp (_rigidbody.position.x, _mapBoundaries.left, _mapBoundaries.right);
			var yPos = Mathf.Clamp (_rigidbody.position.y, _mapBoundaries.bottom, _mapBoundaries.top);
			_rigidbody.position = new Vector2(xPos, yPos);
		}

		void OnDestroy() {
			Unsubscribe();
		}

		public void Subscribe() {
			_controlls.Move += Movement;
		}

		public void Unsubscribe() {
			_controlls.Move -= Movement;
		}

	    public override void Destroy() {
	        base.Destroy();
            Game.Instance.GameLost();
	    }
	}
}