﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MiniRangers {
	public class ScoreManager : ISavable {
		const int LEADERBOARD_MAX_COUNT = 10;

		public uint score { get { return _currentScore; } }

		public delegate void ScoreUpdateDelegate(uint newScore);
		public event ScoreUpdateDelegate ScoreUpdate;

		private uint _currentScore;
		private List<ScoreEntity> _leaderBoard;

		public ScoreManager() {
			_leaderBoard = new List<ScoreEntity>();
			SaveManager.Add(Save, SaveManager.EventType.SAVE);
			SaveManager.Add(Load, SaveManager.EventType.LOAD);
			SaveManager.Add(Loaded, SaveManager.EventType.LOADED);
		}

		public IList<ScoreEntity> GetLeaderBoard() {
			Debug.Log (_leaderBoard.Count);
			return _leaderBoard.AsReadOnly();
		}

		public void AddScore(uint amount) {
			_currentScore += amount;
			if(ScoreUpdate != null) {
				ScoreUpdate(_currentScore);
			}
		}

		public void AddHighScore() {
			if(_currentScore > 0) {
				var newScoreEntity = new ScoreEntity (Game.Instance.userProfile.name, _currentScore);
				_leaderBoard.Add (newScoreEntity);
				SortLeaderBoard();
			}
		}

		public void ResetScore() {
			_currentScore = 0;
		}
	
	#region Save/Load
		public void Save(Hashtable toSave) {
			toSave.Add("leaderBoardCount", _leaderBoard.Count);
			for(int i = 0; i < LEADERBOARD_MAX_COUNT && i< _leaderBoard.Count; ++i) {
				var entity = _leaderBoard[i];
				toSave.Add("user" + i.ToString(), entity.name);
				toSave.Add("score" + i.ToString(), entity.score);
			}
		}

		public void Load(Hashtable toLoad) {
			_leaderBoard.Clear();
			int count = (int) toLoad["leaderBoardCount"];
			for(int i = 0; i < count; ++i) {
				string name = (string)toLoad["user" + i.ToString()];
				uint score = (uint)toLoad["score" + i.ToString()];
				_leaderBoard.Add(new ScoreEntity(name, score));
			}
			SortLeaderBoard();
		}

		public void Loaded(Hashtable toLoad) {
		}
	#endregion

		private void SortLeaderBoard() {
			_leaderBoard.Sort( (se1, se2) => -se1.score.CompareTo(se2.score));
		}

	#region testing
		public IList<ScoreEntity> GetTestLeaderBoard() {
			var list = new List<ScoreEntity>() {
				new ScoreEntity("ZUI", 25683),
				new ScoreEntity("Sam", 3467),
				new ScoreEntity("aasdasdas", 34562),
				new ScoreEntity("blasdasd", 106890),
				new ScoreEntity("A", 100235),
				new ScoreEntity("gogi", 100),
				new ScoreEntity("s", 10),
				new ScoreEntity("HUI", 1533)
			};
			list.Sort( (se1, se2) => -se1.score.CompareTo(se2.score));
			return list;
		}
	#endregion
	}
}
