﻿using UnityEngine;

namespace MiniRangers {
	[System.Serializable]
	public class UserProfile {

		public UserProfile(string newName) {
			name = newName;
		}

		public string name {get; private set;}
	}
}