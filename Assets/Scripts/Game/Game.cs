﻿using System;
using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class Game : Singleton<Game>, ISavable {

		public const string DEFAULT_USER_NAME = "Captain";

		public delegate void UserUpdateDelegate(UserProfile profile);
		public delegate void PauseDelegate(bool isPaused);
	    public event Action GameStarted;
	    public event Action GameEnded;
		public event UserUpdateDelegate UserUpdated;
		public event PauseDelegate Paused;

		public UserProfile userProfile { get; private set; }
		public ScoreManager scoreManager { get; private set;}
		public bool isPaused { get; private set; }

		public Game() {
			scoreManager = new ScoreManager();
			userProfile = new UserProfile(DEFAULT_USER_NAME);
			SaveManager.Add(Save, SaveManager.EventType.SAVE);
			SaveManager.Add(Load, SaveManager.EventType.LOAD);
			SaveManager.Add(Loaded, SaveManager.EventType.LOADED);
		}

		void Start() {
			SoundManager.Instance.PlayMusic(MusicType.MainMenu);
			SaveManager.Load();

			//workaround for the cursor in standalone/Web builds
			Cursor.SetCursor(Resources.Load<Texture2D>("Sprites/cursor"), Vector2.zero, CursorMode.Auto);
		}

	#region public methods
		public void SetUser(string name) {
			if(name == "") return;
			userProfile = new UserProfile(name);
			if(UserUpdated != null) {
				UserUpdated(userProfile);
			}
		}

		public void NewGame() {
			SoundManager.Instance.PlayMusic(MusicType.Battle);
			scoreManager.ResetScore();
			LevelManager.LoadLevel(Levels.Game);
		}

		public void ShowHightScore() {
			LevelManager.LoadLevel(Levels.HighScore);
		}

		public void MainMenu() {
			/*if(LevelManager.currentLevel == Levels.Game) {
				scoreManager.AddHighScore();
			}*/
			SoundManager.Instance.PlayMusic(MusicType.MainMenu);
			LevelManager.LoadLevel(Levels.MainMenu);
		}

		public void Pause(bool set) {
			if(isPaused == set) return;
			isPaused = set;
			Time.timeScale = isPaused ? 0 : 1;
			if(Paused != null) {
				Paused(isPaused);
			}
		}

		public void OnLevelWasLoaded() {
			if(isPaused) {
				Pause(false);
			}
		    if (LevelManager.currentLevel == Levels.Game) {
		        if (GameStarted != null) {
		        GameStarted();
		        }
		    }
		}

	    public void GameLost() {
            scoreManager.AddHighScore();
	        if (GameEnded != null) GameEnded();
            SaveManager.Save();
	    }

	#endregion

	#region Save/Load
		public void Save(Hashtable toSave) {
			toSave.Add("userName", userProfile.name);
		}

		public void Load(Hashtable toLoad) {
			SetUser((string)toLoad["userName"]);
		}

		public void Loaded(Hashtable toLoad) {
			
		}

		private void OnApplicationPause() {
			SaveManager.Save();
		}

		public override void OnApplicationQuit() {
			base.OnApplicationQuit();
			SaveManager.Save();	
		}
	#endregion
	}
}