﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace MiniRangers {
	public enum MusicType {
		MainMenu,
		Battle
	}

	public enum SoundType {
		None,
		Shot1,
		Shot2,
		Explosion
	}

	[Serializable]
	public sealed class MusicClip {
		public MusicType type;
		public AudioClip clip;
	}

	[Serializable]
	public sealed class SoundClip {
		public SoundType type;
		public AudioClip clip;
	}

	[RequireComponent (typeof(AudioListener))]
	public class SoundManager : Singleton<SoundManager> {

		public AudioClip blankSound;
		public List<MusicClip> music = new List<MusicClip>();
		public List<SoundClip> sounds = new List<SoundClip>();

		private AudioSource _musicSource;

		public override void Awake() {
			base.Awake();
			if(GetComponent<AudioListener>() == null) {
				gameObject.AddComponent<AudioListener>();
			}
			var musicSourceObject = new GameObject("MusicSource");
			musicSourceObject.transform.parent = transform;
			_musicSource = musicSourceObject.AddComponent<AudioSource>();
			_musicSource.volume = 0.4f;
		}

	#region music
		public void PlayMusic(MusicType type) {
			_musicSource.clip = GetAudioClip(type);
			if(_musicSource.clip != null) {
				_musicSource.Play();
			}
		}

		private AudioClip GetAudioClip(MusicType type) {
			foreach(var musicClip in music) {
				if(musicClip.type == type) {
					return musicClip.clip;
				}
			}
			return blankSound;
		}

	#endregion

	#region sounds
		public AudioClip GetSound(SoundType type) {
			foreach(var soundClip in sounds) {
				if(soundClip.type == type) {
					return soundClip.clip;
				}
			}
			return blankSound;
		}

	#endregion

	}
}