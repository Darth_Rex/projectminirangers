﻿using UnityEngine;

namespace MiniRangers {
	public class LevelManager {

		public static Levels currentLevel { get; private set; }

		public static void LoadLevel(Levels level) {
			currentLevel = level;
			Application.LoadLevel(GetLevelString(level));
		}

		private static string GetLevelString(Levels level) {
			switch(level) {
			case Levels.MainMenu:
				return "MainMenu";
			case Levels.HighScore:
				return "HighScore";
			case Levels.Game:
				return "Main";
			default:
				Debug.LogException(new System.ArgumentNullException("There is no level" + level));
				return "";
			}
		}
	}
}