using UnityEngine; 

namespace MiniRangers {
	public class Layers	{
		public static int Default = FindLayer("Default");
		public static int UI = FindLayer("UI");
		public static int Player = FindLayer("Enemy");
		public static int Enemy = FindLayer("Enemy");
		public static int Projectile = FindLayer("Enemy");

		public static int PlayerMask = 1 << Player;
		public static int EnemyMask = 1 << Enemy;
		public static int ProjectileMask = 1 << Projectile;

		private static int FindLayer(string name) {
			int num = LayerMask.NameToLayer(name);
			if (num == -1)
				Debug.LogError("Could not find layer '" + name + "'.");
			return num;
		}
	}
}