﻿using UnityEngine;

namespace MiniRangers {
	public class ScoreEntity  {

		public ScoreEntity(string userName, uint newScore) {
			name = userName;
			score = newScore;
		}

		public string name { get; private set; }
		public uint score { get; private set; }
	}
}