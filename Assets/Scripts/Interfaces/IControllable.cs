﻿public interface IControllable  {

	void Subscribe ();
	void Unsubscribe ();
}
