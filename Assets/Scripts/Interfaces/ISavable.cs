﻿using System.Collections;

public interface ISavable {
	void Save(Hashtable toSave);
	void Load(Hashtable toLoad);
	void Loaded(Hashtable noLoad);
}
