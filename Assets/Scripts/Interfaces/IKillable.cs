﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public interface IKillable {
		void ReciveDamage(float amount);
		void Kill();
	}
}