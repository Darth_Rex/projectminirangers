using UnityEngine;
using System.Collections;

public class SaveManager {

	public enum EventType {
		SAVE,
		LOAD,
		LOADED
	}

	public delegate void OnSaveLoadDelegate(Hashtable toSaveLoad);
	private static event OnSaveLoadDelegate OnSave;
	private static event OnSaveLoadDelegate OnLoad;
	private static event OnSaveLoadDelegate OnLoaded;

	const string SAVE_FILE_NAME = "save.bin";
	static bool _isLoaded = false;

	public static void Save() {
		if(!_isLoaded) {
			Debug.LogWarning("SaveManager: game was not loaded yet");
			return;
		}

		var toSave = new Hashtable();

		if (OnSave != null) {
			OnSave(toSave);
		}
		BinarySaver.Save (toSave, SAVE_FILE_NAME);
		Debug.Log("SaveManager: Game was saved to " + GetSavePath());
	}
	
	public static void Load() {
		Debug.Log("SaveManager.Load()");

		var toLoad = BinarySaver.Load (SAVE_FILE_NAME) as Hashtable;;
		if (toLoad == null) {
			Debug.Log ("No Save was found. New Game");
		} else { 
			if (OnLoad != null) {
				OnLoad (toLoad);
			}
		}
		_isLoaded = true;
		if (OnLoaded != null) {
			OnLoaded (toLoad);
		}
	}

	public static void Add(OnSaveLoadDelegate eventDelegate, EventType eventType) {
		switch(eventType) {
		case EventType.SAVE:
			OnSave += eventDelegate;
			break;
		case EventType.LOAD:
			OnLoad += eventDelegate;
			break;
		case EventType.LOADED:
			OnLoaded += eventDelegate;
			break;
		}
	}

	public static void Remove(OnSaveLoadDelegate eventDelegate, EventType eventType) {
		switch(eventType) {
		case EventType.SAVE:
			OnSave -= eventDelegate;
			break;
		case EventType.LOAD:
			OnLoad -= eventDelegate;
			break;
		case EventType.LOADED:
			OnLoaded -= eventDelegate;
			break;
		}
	}

    public static string GetSavePath() {
		return Application.persistentDataPath + "/" + SAVE_FILE_NAME;
    }
}
