﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class BinarySaver
{
	public static void Save(object obj, string fileName) {
		string _fullPath = Application.persistentDataPath + "/" + fileName;
		FileStream fs = new FileStream(_fullPath, FileMode.Create);
		
		BinaryFormatter formatter = new BinaryFormatter();
		try {
		    formatter.Serialize(fs, obj);
		}
		catch (SerializationException e) {
			Debug.LogException(e);
		    throw;
		}
		finally {
		    fs.Close();
		}
	}
	
	public static object Load(string fileName)
	{
		string _fullPath = Application.persistentDataPath + "/" + fileName;
		if (!File.Exists(_fullPath)) return null;
		
		FileStream fs = new FileStream(_fullPath, FileMode.Open);
		object obj = null;
		try {
		 	BinaryFormatter formatter = new BinaryFormatter();
		 	obj = (object)formatter.Deserialize(fs);
		}
		catch (SerializationException e) {
			Debug.LogException(e);
		    throw;
		}
		finally {
			Debug.Log("File was loaded from: " + _fullPath);
			fs.Close();
		}
		return obj;
	}
}