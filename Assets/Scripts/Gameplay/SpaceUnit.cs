﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class SpaceUnit : SpaceEntity, ISpawnable {

		public float speed;

		protected Rigidbody2D _rigidbody;
		protected Transform _transform;
		
		public virtual void Awake() {
			_transform = transform;
			_rigidbody = GetComponent<Rigidbody2D>();
		}

		public virtual void OnSpawn() {
		}
		
		public virtual void OnDespawn() {
		}
	}
}