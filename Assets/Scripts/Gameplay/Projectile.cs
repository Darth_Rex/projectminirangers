﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	[RequireComponent (typeof(Rigidbody2D))]
	public class Projectile : SpaceUnit {

	    public LayerMask hitMask {get; private set;}
		public float damage {get; private set;}
		public float lifeTime;

		private float _timeBeforeDespawn;
		private AudioSource _audioSource;

		public void SetValues(float newSpeed, float newDamage, LayerMask newMask, Fraction projectileFraction) {
			speed = newSpeed;
			damage = newDamage;
			hitMask = newMask;
			fraction = projectileFraction;
			LaunchProjectile();
		}

		public override void OnSpawn() {
			base.OnSpawn();
			if(_audioSource.clip != null) {
				_audioSource.Play();
			}
		}

		protected void LaunchProjectile() {
            StopCoroutine("LifetimeCycle");
            StartCoroutine("LifetimeCycle", lifeTime);
			_rigidbody.velocity = transform.up * speed;
			if(fraction == Fraction.Human) _audioSource.clip = SoundManager.Instance.GetSound(SoundType.Shot2);
			else _audioSource.clip = SoundManager.Instance.GetSound(SoundType.Shot1);
			_audioSource.Play();
		}

		public override void Awake () {
			base.Awake();
			_audioSource = GetComponent<AudioSource>();
		}

		void OnEnable() {
			LaunchProjectile();
		}

		void OnDisable() {
			if(_timeBeforeDespawn > 0) {
				StopCoroutine("LifetimeCycle");
			}
		}

		IEnumerator LifetimeCycle(float time) {
			_timeBeforeDespawn = time;
			while (_timeBeforeDespawn > 0) {
				yield return null;
				_timeBeforeDespawn -= Time.deltaTime;
			}
			gameObject.Recycle();
		}
	}
}