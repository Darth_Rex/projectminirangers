﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class Weapon : SpaceEntity {
		//public Fraction fraction;
		public LayerMask hitMask;
		public float damage;
		public float projectileSpeed;
	    public float fireDelay;

		protected Transform _transform;
		protected bool _isShooting;

		private float _fireCountdown;

		public virtual void Awake() {
			_transform = transform;
			_isShooting = false;
		}

		public virtual void Shoot() {
			Shoot(_transform.up);
		}

		public virtual void Shoot(Vector3 direction) {
			var prefab = ObjectPoolCollection.Instance.GetObjectPoolPrefab(CollectionEntityType.LaserShot);
			var bullet = prefab.Spawn(_transform.position).GetComponent<Projectile>();
			bullet.transform.up = direction.normalized;
			bullet.SetValues(projectileSpeed, damage, hitMask, fraction);
		}

		public virtual void Update() {
			if(_isShooting) {
				var time = Time.time;
				if(time >= _fireCountdown) {
					_fireCountdown = time + fireDelay;
					Shoot();
				}
			}
		}
	}
}