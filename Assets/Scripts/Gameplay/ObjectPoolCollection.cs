﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public enum CollectionEntityType {
		Asteroid,
		PlayerShip,
		EnemyShip,
		Explosion,
		LaserShot
	}

	[System.Serializable]
	public class CollectionEntity {
		public CollectionEntityType type;
		public GameObject prefab;
	}

	public class ObjectPoolCollection : SceneSingleton<ObjectPoolCollection> {
		public CollectionEntity[] entities;

		public GameObject GetObjectPoolPrefab(CollectionEntityType type) {
			foreach(var ent in entities) {
				if(ent.type == type) {
					return ent.prefab;
				}
			}
			return null;
		}
	}
}