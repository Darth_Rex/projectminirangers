﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public class Boundaries {

		public float left {get; private set;}
		public float right {get; private set;}
		public float top {get; private set;}
		public float bottom {get; private set;}

		public Boundaries () {
			Vector2 bottomCorner = Camera.main.ViewportToWorldPoint(Vector3.zero);
			Vector2 topCorner = Camera.main.ViewportToWorldPoint(Vector3.one);
			left = bottomCorner.x;
			right = topCorner.x;
			bottom = bottomCorner.y;
			top = topCorner.y;
		}

		public Boundaries(BoxCollider2D includeCollider) {
			Vector2 bottomCorner = Camera.main.ViewportToWorldPoint(Vector3.zero);
			Vector2 topCorner = Camera.main.ViewportToWorldPoint(Vector3.one);
			left = bottomCorner.x + includeCollider.bounds.extents.x;
			right = topCorner.x - includeCollider.bounds.extents.x;
			bottom = bottomCorner.y + includeCollider.bounds.extents.y;
			top = topCorner.y - includeCollider.bounds.extents.y;
		}

		public override string ToString ()
		{
			return string.Format ("[Boundaries: left={0}, right={1}, top={2}, bottom={3}]", left, right, top, bottom);
		}
	}
}