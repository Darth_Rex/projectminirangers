﻿using UnityEngine;
using System.Collections;

public class ParallaxScrollSprite : MonoBehaviour {

	public float speed = 0.5f;
	public Transform secondBack;

	private Transform _transform;
	private Vector3 _initialPos;
	private float _offsetMagnitude;

	void Start () {
		_transform = transform;
		_initialPos = _transform.position;
		_offsetMagnitude = (secondBack.position - _transform.position).magnitude;
	}
	
	void Update () {
		Vector3 pos = _transform.position;
		pos.y -= Time.deltaTime * speed;
		if(Mathf.Abs(pos.y - _initialPos.y) >= _offsetMagnitude) {
			pos.y += _offsetMagnitude;
		}
		_transform.position = pos;
	}
}
