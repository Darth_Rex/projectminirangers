﻿using UnityEngine;
using System.Collections;

public class ParallaxScrollUV : MonoBehaviour {

	public float parallaxSpeed;

	private Material _material;

	void Start() {
		_material = GetComponent<MeshRenderer>().material;
	}

	void Update () {
		Vector2 offset = _material.mainTextureOffset;
		offset.y += Time.deltaTime * parallaxSpeed;
		_material.mainTextureOffset = offset;
	}
}
