﻿using UnityEngine;
using System.Collections;

namespace MiniRangers {
	public abstract class Ship : SpaceUnit {

		public float maxHitPoints;
		public float currentHitPoints { get; private set; }
		
		public virtual void Start() {
			ResetStats();
		}
		
		public virtual void IncomingDamage(float damage) {
			if(damage < 0) {
				Debug.LogWarning("Incoming damage cannot be below 0");
				return;
			}
			currentHitPoints -= damage;
			if(currentHitPoints <= 0) {
				Destroy();
			}
		}
		
		public virtual void Destroy() {
			gameObject.Recycle();
			ObjectPoolCollection.Instance.GetObjectPoolPrefab(CollectionEntityType.Explosion).Spawn(_transform.position);
		}
		
		public void OnTriggerEnter2D(Collider2D other) {
			var projectile = other.GetComponent<Projectile>();
			if (projectile != null) {
				if ((projectile.hitMask.value & (1 << gameObject.layer)) != 0) {
					IncomingDamage(projectile.damage);
					projectile.Recycle();
				}
			}
		}
		
		public void ResetStats() {
			if(maxHitPoints <= 0) {
				Debug.LogError("maxHitPoints must be above 0, setting it to 1");
				maxHitPoints = 1f;
			}
			currentHitPoints = maxHitPoints;
		}
		

	}
}